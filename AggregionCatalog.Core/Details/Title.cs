﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggregionCatalog.Core.Details
{
    public class Title
    {
        [JsonProperty("default")]
        public string Default { get;  set; }

        public override string ToString()
        {
            return this.Default;
        }
    }
}
