﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggregionCatalog.Core.Details
{
    public class Options
    {
        [JsonProperty("authors")]
        public string Authors { get; set; }

        [JsonProperty("ISBN")]
        public string ISBN { get; set; }

        [JsonProperty("publisher")]
        public string Publisher { get; set; }

        public override string ToString()
        {
            return this.Authors + " " + this.ISBN + " " + this.Publisher;
        }
    }
}
