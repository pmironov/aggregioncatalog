﻿using AggregionCatalog.Core.Classes;
using AggregionCatalog.Core.Models;
using AggregionCatalog.Core.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace AggregionCatalog.Core.ViewModels
{
    public class BookViewModel : INotifyPropertyChanged
    {
        private JsonService mJsonService;
        public BookViewModel()
        {
            mCanExecute = true;
            mJsonService = new JsonService();
            Init();            
        }

        /// <summary>
        /// Инициализация списка при создании ViewModel
        /// </summary>
        /// <returns></returns>
        private async Task Init() 
        {
            Books = await mJsonService.GetAsync<ObservableCollection<BookModel>>("http://ds.aggregion.com/api/public/catalog/");
        }
        
        private ObservableCollection<BookModel> mBooks;
        /// <summary>
        /// Список книг
        /// </summary>
        public ObservableCollection<BookModel> Books
        {
            get 
            {
                if (mBooks == null) mBooks = new ObservableCollection<BookModel>();
                return mBooks;
            }
            set
            {
                mBooks = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Books"));
            }
        }

        private ICommand mSearchCommand;
        /// <summary>
        /// Обработчик команды поиска
        /// </summary>
        public ICommand SearchCommand
        {
            get
            {
                return mSearchCommand ?? (mSearchCommand = new CommandHandler(async () => await SearchAsync(), mCanExecute));
            }
        }

        private bool mCanExecute;

        private string mSearchString;
        /// <summary>
        /// Текст для поиска
        /// </summary>
        public string SearchString
        {
            get { return mSearchString; }
            set
            {
                mSearchString = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("SearchString"));
            }
        }
        
        private async Task SearchAsync()
        {
            // создаём строку поиска
            var url = string.Format("http://ds.aggregion.com/api/public/catalog?filter=title.default(\"{0}\",regex)", mSearchString);
            // загружаем элементы
            Books = await mJsonService.GetAsync<ObservableCollection<BookModel>>(url);
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
