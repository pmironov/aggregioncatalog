﻿using AggregionCatalog.Core.Details;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggregionCatalog.Core.Models
{
    public class BookModel
    {
        [JsonProperty("title")]
        public Title Title { get; set; }

        [JsonProperty("description")]
        public Description Description { get; set; }

        [JsonProperty("cover")]
        public string Cover { get; set; }

        [JsonProperty("options")]
        public Options Options { get; set; }

        [JsonProperty("owner")]
        public string Owner { get; set; }

        [JsonProperty("licenseParams")]
        public List<string> LicenseParams { get; set; }

        [JsonProperty("contentType")]
        public string ContentType { get; set; }

        [JsonProperty("publishingStatus")]
        public string PublishingStatus { get; set; }

        [JsonProperty("maxKeys")]
        public long MaxKeys { get; set; }

        [JsonProperty("previewVideos")]
        public List<string> PreviewVideos { get; set; }

        [JsonProperty("previewImages")]
        public List<string> PreviewImages { get; set; }

        [JsonProperty("platformSupport")]
        public List<string> PlatformSupport { get; set; }

        [JsonProperty("tags")]
        public List<string> Tags { get; set; }

        [JsonProperty("id")]
        public string ID { get; set; }
        
        public override string ToString()
        {
            return this.Title.Default;
        }
    }
}
