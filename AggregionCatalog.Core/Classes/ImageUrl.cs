﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AggregionCatalog.Core.Classes
{
    public class ImageUrl
    {
        public string ImagePath = string.Empty;
        public ImageUrl(string resourceId)
        {
            // адрес для запроса был взят с https://confluence.aggregion.com/display/UG/File+storage+management (единственный Public метод оттуда)
            var pathOnStorage = string.Format("https://storage.aggregion.com/api/files/{0}/shared/data", resourceId);
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(pathOnStorage);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader sr = new StreamReader(stream);
                var res = sr.ReadToEnd();
            }
            catch
            {
                // здесь пусто, потому что сервер постоянно шлёт 404, поэтому все изображения заменены заглушкой
            }
            ImagePath = "http://icons.veryicon.com/ico/Movie%20%26%20TV/Futurama%20Vol.%201/Bender.ico";
        }
    }
}
