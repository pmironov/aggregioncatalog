﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AggregionCatalog.Core.Services.Interfaces
{
    public interface IJsonService
    {
        /// <summary>
        /// Запрос данных с сервера
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string requestUrl);
    }
}
