﻿using AggregionCatalog.Core.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AggregionCatalog.Core.Services
{
    public class JsonService : IJsonService
    {
        /// <summary>
        /// Запрос данных с сервера
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="requestUrl"></param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(string requestUrl)
        {
            var request = WebRequest.Create(new Uri(requestUrl)) as HttpWebRequest;
            try
            {
                request.Method = "GET";
                request.Accept = "application/json";
                // ждём ответа сервера
                WebResponse responseObject = await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, request);
                using (var responseStream = responseObject.GetResponseStream())
                {
                    // читаем и десериализуем
                    var sr = new StreamReader(responseStream);
                    var received = await sr.ReadToEndAsync();
                    var result = JsonConvert.DeserializeObject<T>(received);
                    return (T)result;
                }
            }
            catch (Exception ex) 
            {
                System.Diagnostics.Debug.Write(ex.ToString());
                // возвращаем путой объект в случае ошибки
                return default(T);
            }
            finally
            {
                request = null;
            }
        }
    }
}
