﻿using AggregionCatalog.Core.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace AggregionCatalog.Classes
{
    public class ImageConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // исходя из документации, по запросу вида https://storage.aggregion.com/api/files/{resourceId}/shared/data мы можем получить ссылку на файл
            // проверить не удалось из-за ошибки 404, которую отправляет сервер
            var imagePath = new ImageUrl((string)value).ImagePath;
            return new BitmapImage(new Uri(imagePath));
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}
